//  File name   : Model.swift
//
//  Author      : Dung Vu
//  Created date: 11/20/18
//  Version     : 1.00
//  --------------------------------------------------------------
//  Copyright © 2018 Dung Vu. All rights reserved.
//  --------------------------------------------------------------

import Foundation

/// Use For ObjC
@objcMembers
public final class ModelPhone: NSObject, PhoneFormatProtocol {
    public private(set) var prefix: String
    public private(set) var originalPhone: String
    public private(set) var internationalPhone: String
    
    public init(with prefix: String, originalPhone: String, internationalPhone: String) {
        self.prefix = prefix
        self.originalPhone = originalPhone
        self.internationalPhone = internationalPhone
        super.init()
    }
}

enum ServiceConfig: Int, Codable {
    case thirdParty = 0
    case internalAPI = 1
}

extension ServiceConfig {
    var authenService: AuthenProtocol {
        switch self {
        case .thirdParty:
            return ThirdPartySMSAuthen()
        case .internalAPI:
            return VatoSMSAuthen()
        }
    }
}

struct RequestCode: Codable {
    let sessionId: String
    let nextService: ServiceConfig
    let retryAt: TimeInterval
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        sessionId = try values.decode(String.self, forKey: .sessionId)
        nextService = try values.decode(ServiceConfig.self, forKey: .nextService)
        if let t = try values.decodeIfPresent(TimeInterval.self, forKey: .retryAt) {
            let delta = Date(timeIntervalSince1970: t / 1000).timeIntervalSinceNow
            retryAt = abs(delta)
        } else {
            retryAt = 60
        }
    }
}

struct RequestCustomToken: Codable {
    let customToken: String
}

struct RequestAuthenticateConfig: Codable {
    let defaultService: ServiceConfig
}

struct RequestConfig: Codable {
    let authenticate: RequestAuthenticateConfig
}


