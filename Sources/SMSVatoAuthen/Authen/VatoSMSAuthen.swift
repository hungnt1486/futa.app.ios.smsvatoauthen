//  File name   : VatoSMSAuthen.swift
//
//  Author      : Dung Vu
//  Created date: 11/20/18
//  Version     : 1.00
//  --------------------------------------------------------------
//  Copyright © 2018 Dung Vu. All rights reserved.
//  --------------------------------------------------------------

import Foundation
import RxSwift
import VatoNetwork
import Alamofire
import UIKit

struct VatoSMSAuthen: AuthenProtocol {
    var isInternalAPI: Bool {
        return true
    }
    
    private var sessionId: String = ""
    private let key = "VatoSMSAuthen"
    
    private func uuid() -> String {
        let userDefault = UserDefaults.standard
        guard let value = userDefault.object(forKey: key) as? String, !value.isEmpty  else {
            // generate
            let n = UIDevice.current.identifierForVendor?.uuidString ?? ""
            // Save
            defer {
                if !n.isEmpty {
                    userDefault.set(n, forKey: key)
                    userDefault.synchronize()
                }
            }
            return n
        }
        return value
    }
    
    func authen(phone: PhoneFormatProtocol) -> Observable<String> {
        defer { SMSVatoAuthenInterface.dependency?.authenTracking(service: 1) }
        
        var current = phone.originalPhone
        if current.hasPrefix("0") {
           let temp = String(current.suffix(max(current.count - 1, 0)))
           current = "\(phone.prefix)\(temp)"
        }
        debugPrint("Phone Change: \(current)!!!!")
        
        let deviceId = self.uuid()
        debugPrint("deviceId: \(deviceId)!!!!")
        let router = VatoAPIRouter.authenticateRequestCode(phone: current, deviceId: deviceId)
        let r: Observable<(HTTPURLResponse, OptionalMessageDTO<RequestCode>)> =  Requester.requestDTO(using: router, method: .post, encoding: JSONEncoding.default, block: nil)
        return r.catchError({ (e)  in
            SMSVatoAuthenInterface.canUseInternalAPI = self.canReTry(from: e)
            return Observable.error(e)
        }).do(onNext: { (r) in
            // Cache For next service
            SMSVatoAuthenInterface.retry = r.1.data?.retryAt ?? 60
            SMSVatoAuthenInterface.nextService = r.1.data?.nextService
        }).map {
            if let e = $0.1.error {
                SMSVatoAuthenInterface.canUseInternalAPI = self.canReTry(from: e)
                throw NSError(message: $0.1.message)
            }
            return $0.1.data?.sessionId ?? ""
        }
    }
    
    func authen(otp: String) -> Observable<User?> {
        let router = VatoAPIRouter.authenticateVerifyCode(sms: otp, sessionId: self.sessionId)
        let r: Observable<(HTTPURLResponse, OptionalMessageDTO<RequestCustomToken>)> = Requester.requestDTO(using: router, method: .post, encoding: JSONEncoding.default, block: nil)
        return r.catchError({ (e)  in
            SMSVatoAuthenInterface.canUseInternalAPI = self.canReTry(from: e)
            return Observable.error(e)
        }).map({ (r) -> RequestCustomToken in
            if r.1.fail {
                SMSVatoAuthenInterface.canUseInternalAPI = self.canReTry(from: r.1.error)
                let e = NSError(message: r.1.message)
                throw e
            } else {
                guard let result = r.1.data else {
                    let e = NSError(domain: NSURLErrorDomain, code: NSURLErrorBadServerResponse, userInfo: [NSLocalizedDescriptionKey : "Can't Check"])
                    SMSVatoAuthenInterface.canUseInternalAPI = self.canReTry(from: r.1.error)
                    throw r.1.error ?? e
                }
                return result
            }
        }).flatMap { self.validate(from: $0) }
    }
    
    private func validate(from response: RequestCustomToken) -> Observable<User?> {
        guard let dependency = SMSVatoAuthenInterface.dependency else {
            fatalError("Please Setting dependency")
        }
        
        return Observable<User?>.create({ (s) -> Disposable in
            dependency.authen(customToken: response.customToken, complete: { (u) in
                s.onNext(u)
                s.onCompleted()
            }, error: { (e) in
                s.onError(e)
            })
            return Disposables.create()
        })
    }
    
    mutating func update(verifyCode: String) {
        self.sessionId = verifyCode
    }
    
    private func canReTry(from error: Error?) -> Bool {
        guard error != nil else {
            return true
        }
        return false
        
//        debugPrint("Authenticate Error: \(error.localizedDescription)")
//
//        let m = (error.localizedDescription.components(separatedBy: ".").last ?? "").lowercased()
//
//        switch m {
//        case "ReachOutOfSmsServiceException".lowercased():
//            return false
//        case "InvalidVerifyCodeToVerifyAuthenticateException".lowercased():
//            return true
//        case "InvalidIpAddressToVerifyAuthenticateException".lowercased():
//            return false
//        case "VerifyCodeTimeOutException".lowercased():
//            return false
//        case "ReachMaxAttemptVerifyAuthenticateException".lowercased():
//            return false
//        case "FailedToConnectToAuthenticateServiceException".lowercased():
//            return true
//        case "ReachMaxRequestAuthenticateException".lowercased():
//            return false
//        case "MustWaitBeforeRequestAnotherVerifyCodeException".lowercased():
//            return true
//        case "InvalidPhoneNumberFormatException".lowercased():
//            return true
//        case "FailedToConnectToSmsServiceException".lowercased():
//            return false
//        default:
//            return true
//        }
    }
}

