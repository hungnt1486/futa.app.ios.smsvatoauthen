//  File name   : ThirdPartySMSAuthen.swift
//
//  Author      : Dung Vu
//  Created date: 11/20/18
//  Version     : 1.00
//  --------------------------------------------------------------
//  Copyright © 2018 Dung Vu. All rights reserved.
//  --------------------------------------------------------------

import Foundation
import RxSwift
struct ThirdPartySMSAuthen: AuthenProtocol {
    var isInternalAPI: Bool {
        return false
    }
    
    private var verificationCode: String = ""
    func authen(phone: PhoneFormatProtocol) -> Observable<String> {
        guard let dependency = SMSVatoAuthenInterface.dependency else {
            fatalError("Please Setting dependency")
        }
        defer { dependency.authenTracking(service: 0) }
        return Observable<String>.create({ (s) -> Disposable in
            dependency.authen(phone: phone.internationalPhone, complete: { (v) in
                s.onNext(v)
                s.onCompleted()
            }, error: { (e) in
                s.onError(e)
            })
            return Disposables.create()
        })
    }
    
    func authen(otp: String) -> Observable<User?> {
        guard let dependency = SMSVatoAuthenInterface.dependency else {
            fatalError("Please Setting dependency")
        }
        return Observable<User?>.create({ (s) -> Disposable in
            dependency.authen(otp: otp, use: self.verificationCode, complete: {
                s.onNext($0)
                s.onCompleted()
            }, error: { (e) in
                s.onError(e)
            })
            return Disposables.create()
        })
    }
    
    mutating func update(verifyCode: String) {
        verificationCode = verifyCode
    }
    
}

