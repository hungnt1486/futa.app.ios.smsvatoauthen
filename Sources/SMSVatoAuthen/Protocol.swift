//  File name   : Protocol.swift
//
//  Author      : Dung Vu
//  Created date: 11/20/18
//  Version     : 1.00
//  --------------------------------------------------------------
//  Copyright © 2018 Dung Vu. All rights reserved.
//  --------------------------------------------------------------

import Foundation
import VatoNetwork
import RxSwift

// MARK: Phone
@objc public protocol PhoneFormatProtocol: NSObjectProtocol {
    var prefix: String { get }
    var originalPhone: String { get }
    var internationalPhone: String { get }
}

@objc public protocol UserProtocol {}
public protocol AuthenProtocol {
    // Veritifycode
    var isInternalAPI: Bool { get }
    func authen(phone: PhoneFormatProtocol) -> Observable<String>
    func authen(otp: String) -> Observable<User?>
    mutating func update(verifyCode: String)
}

public typealias User = UserProtocol
public typealias ProviderAuthenCompleteHandler = (String) -> ()
public typealias ProviderAuthenUserCompleteHandler = (User?) -> ()
public typealias ProviderAuthenErrorHandler = (Error) -> ()

/// Protocol dependency by third party
@objc public protocol AuthenDependencyProtocol: AnyObject {
    /// Dependency third party authenticate phone
    ///
    /// - Parameters:
    ///   - phone: use format international
    ///   - complete: return veritificationID
    ///   - error: error authenticate
    func authen(phone: String, complete: @escaping ProviderAuthenCompleteHandler, error: @escaping ProviderAuthenErrorHandler)
    
    /// Dependency third party authenticate otp
    ///
    /// - Parameters:
    ///   - otp: otp from sms
    ///   - verify: verify code
    ///   - complete: return user for authenticate
    ///   - error: error authenticate
    func authen(otp: String, use verify: String, complete: @escaping ProviderAuthenUserCompleteHandler, error: @escaping ProviderAuthenErrorHandler)
    
    
    /// Dependency third party authenticate custom token
    ///
    /// - Parameters:
    ///   - customToken: string
    ///   - complete: return user for authenticate
    ///   - error: error authenticate
    func authen(customToken: String, complete: @escaping ProviderAuthenUserCompleteHandler, error: @escaping ProviderAuthenErrorHandler)
    
    
    /// Use for analytic
    ///
    /// - Parameter type: type of service 0: Third party service , 1: Internal service
    func authenTracking(service type: Int)
}

// MARK: - Authen Interface

/// Provider interface authenticate
public protocol ProviderAuthenProtocol {
    
    /// Request verification code
    ///
    /// - Parameters:
    ///   - phone: phone input
    ///   - complete: return veritificationID
    ///   - error: error process
    static func authenPhone(with phone: PhoneFormatProtocol, complete: @escaping ProviderAuthenCompleteHandler, error: @escaping ProviderAuthenErrorHandler)
    
    /// Verify OTP
    ///
    /// - Parameters:
    ///   - otp: string sms
    ///   - complete: return user for authenticated
    ///   - error: error authenticate
    static func authenOTP(with otp: String, complete: @escaping ProviderAuthenUserCompleteHandler, error: @escaping ProviderAuthenErrorHandler)
    
    static func retrySendSMS(complete: @escaping ProviderAuthenCompleteHandler, error: @escaping ProviderAuthenErrorHandler)
    static func cancelAll()
}

