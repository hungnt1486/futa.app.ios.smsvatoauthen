//  File name   : Request.swift
//
//  Author      : Dung Vu
//  Created date: 11/20/18
//  Version     : 1.00
//  --------------------------------------------------------------
//  Copyright © 2018 Dung Vu. All rights reserved.
//  --------------------------------------------------------------

import Foundation
import RxSwift
import VatoNetwork

extension NSError {
    convenience init(message: String?) {
        self.init(domain: NSURLErrorDomain, code: NSURLErrorUnknown, userInfo: [NSLocalizedDescriptionKey : message ?? ""])
    }
}
/// Interface authenticate
@objcMembers
public final class SMSVatoAuthenInterface: NSObject, ProviderAuthenProtocol {
    struct PhoneDummy {
        // Use for apple review
        static let apple: String = "901234567"
    }
    
    private static var currentAuthen: AuthenProtocol?
    private static var disposeBag: DisposeBag! = DisposeBag()
    internal static var externalAuthen: AuthenProtocol?
    internal static weak var dependency: AuthenDependencyProtocol?
    internal static var canUseInternalAPI: Bool = true
    internal static var phoneCache: PhoneFormatProtocol?
    internal static var nextService: ServiceConfig? {
        didSet {
            debugPrint("Next Service: \(nextService?.rawValue ?? -1)")
        }
    }
    public static internal(set) var retry: TimeInterval = 60
    private override init() {
        super.init()
    }
    
    
    /// Use for authenticate by custom external authenticate
    /// - Parameter custom: object conform protocol `AuthenProtocol`
    public static func setExternalAuthenticate(_ custom: AuthenProtocol) {
        self.externalAuthen = custom
    }
    
    public static func clearExternalAuthenticate() {
        externalAuthen = nil
    }
    
    /// Set dependency because it 'll use third party verify
    ///
    /// - Parameter dependency: instance conform AuthenDependencyProtocol
    public static func configure(dependency: AuthenDependencyProtocol) {
        self.dependency = dependency
    }
    
    private static func fetchConfig() -> Observable<AuthenProtocol> {
        guard canUseInternalAPI else {
            self.canUseInternalAPI = true
            nextService = nil
            return Observable.just(nextService?.authenService ?? ThirdPartySMSAuthen())
        }
        
        let router = VatoAPIRouter.authenticateConfig
        let request: Observable<(HTTPURLResponse, OptionalMessageDTO<RequestConfig>)> = Requester.requestDTO(using: router)
        
        // Only set
        return request.map { (r) throws -> AuthenProtocol in
            if r.1.fail {
                throw NSError(message: r.1.message)
            } else {
                return r.1.data?.authenticate.defaultService.authenService ?? ThirdPartySMSAuthen()
            }
        }
    }
    
    public static func authenPhone(with phone: PhoneFormatProtocol, complete: @escaping ProviderAuthenCompleteHandler, error: @escaping ProviderAuthenErrorHandler) {
        // Begin
        self.phoneCache = phone
        // Check if dummy -> move for firebase
        let eventAuthen: Observable<String>
        if let external = self.externalAuthen {
            currentAuthen = external
            eventAuthen = external.authen(phone: phone)
        } else {
            if phone.originalPhone.contains(PhoneDummy.apple) {
                debugPrint("Firebase Authen!!!!")
                let external = ThirdPartySMSAuthen()
                self.currentAuthen = external
                eventAuthen = external.authen(phone: phone)
            } else {
                debugPrint("Vato Server Authen!!!!")
                eventAuthen = self.fetchConfig().do(onNext: {
                    self.currentAuthen = $0
                }).flatMap { (authen) -> Observable<String> in
                    return authen.authen(phone: phone)
                }
            }
        }
        
        eventAuthen.subscribe(onNext: {
            self.currentAuthen?.update(verifyCode: $0)
            complete($0)
        }, onError: { (e) in
            error(e)
        }, onDisposed: {
            debugPrint("Dispose Authen")
        }).disposed(by: disposeBag)
    }
    
    public static func authenOTP(with otp: String, complete: @escaping ProviderAuthenUserCompleteHandler, error: @escaping ProviderAuthenErrorHandler) {
        
        guard let currentAuthen = currentAuthen else {
            fatalError("Please authen phone")
        }
        
        currentAuthen.authen(otp: otp).subscribe { (e) in
            switch e {
            case .next(let user):
               complete(user)
            case .error(let e):
                error(e)
            case .completed:
                debugPrint("Completed")
            }
        }.disposed(by: disposeBag)
    }
    
    public static func retrySendSMS(complete: @escaping ProviderAuthenCompleteHandler, error: @escaping ProviderAuthenErrorHandler) {
        guard currentAuthen != nil, let phone = self.phoneCache else {
            fatalError("Please authen phone")
        }
        
        let next: AuthenProtocol = externalAuthen ?? nextService?.authenService ?? ThirdPartySMSAuthen()
        nextService = nil
        currentAuthen = next
        next.authen(phone: phone).subscribe(onNext: { (s) in
            self.currentAuthen?.update(verifyCode: s)
            complete(s)
        }, onError: { (e) in
            error(e)
        }).disposed(by: disposeBag)
    }
    
    public static func cancelAll() {
        disposeBag = nil
        // Create new
        disposeBag = DisposeBag()
    }
}
