// swift-tools-version: 5.8
// The swift-tools-version declares the minimum version of Swift required to build this package.

import PackageDescription

let package = Package(
    name: "SMSVatoAuthen",
    products: [
        // Products define the executables and libraries a package produces, and make them visible to other packages.
        .library(
            name: "SMSVatoAuthen",
            targets: ["SMSVatoAuthen"]),
    ],
    dependencies: [
        // Dependencies declare other packages that this package depends on.
        // .package(url: /* package url */, from: "1.0.0"),
        .package(url: "https://gitlab.com/hungnt1486/futa.app.ios.vatonetwork", branch: "main"),
    ],
    targets: [
        // Targets are the basic building blocks of a package. A target can define a module or a test suite.
        // Targets can depend on other targets in this package, and on products in packages this package depends on.
        .target(
            name: "SMSVatoAuthen",
            dependencies: [
                .product(name: "VatoNetwork", package: "futa.app.ios.vatonetwork"),
            ],
            path: "Sources"
        ),
    ]
)
